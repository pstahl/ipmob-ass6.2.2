package com.example.philip.ass622;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import android.provider.Telephony;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;


public class MainActivity extends ListActivity {

    private String[] SMS_FIELDS = {
            Telephony.Sms.ADDRESS,
            Telephony.Sms.BODY,
    };

    private String[] SMS_PROJECTION = {
            Telephony.Sms._ID,
            Telephony.Sms.ADDRESS,
            Telephony.Sms.BODY,
    };

    private final static int[] TO_IDS = {
            R.id.textView_smsAddress,
            R.id.textView_smsBody,
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);


        Cursor query = this.managedQuery(Uri.parse("content://sms/inbox"), SMS_PROJECTION, null, null, null);


        final ListAdapter adapter = new SimpleCursorAdapter(
                this, R.layout.list_row, query,
                SMS_FIELDS,
                TO_IDS);

        ListView listView = (ListView) findViewById(android.R.id.list);

        this.setListAdapter(adapter);
    }
}
